#!/bin/bash
set -ex

. containers/build_functions.sh

build() {
    buildcntr=$(buildah from --isolation=chroot $EXTRA_BUILDAH_FROM_ARGS docker://telegraf:alpine)
    buildmnt=$(buildah mount $buildcntr)

    cp -a containers/telegraf.conf $buildmnt/etc/telegraf/
    cp -a containers/telegraf-extra-inputs.sh $buildmnt/usr/local/bin/telegraf-extra-inputs.sh

    $buildah_run $buildcntr apk --no-cache add smartmontools nvme-cli bash

    buildah config --entrypoint '/usr/bin/telegraf' $buildcntr
}

build_and_push_container
